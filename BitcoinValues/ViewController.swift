//
//  ViewController.swift
//  BitcoinValues
//
//  Created by Ada 2018 on 20/09/2018.
//  Copyright © 2018 Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var moedasPickerView: UIPickerView!
    @IBOutlet weak var consultarButton: UIButton!
    
    var pickerElements: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        moedasPickerView.dataSource = self
        moedasPickerView.delegate = self
        
        consultarButton.backgroundColor = UIColor.yellow
        consultarButton.layer.cornerRadius = 5
        consultarButton.layer.masksToBounds = true
        
        get(url: "https://blockchain.info/ticker") {
            (json) in
            self.pickerElements = Array(json.keys)
            DispatchQueue.main.async {
                self.moedasPickerView.reloadAllComponents()
            }
        }
        
        consultarButton.addTarget(self, action: #selector(consultarMoedaAction), for: .touchUpInside)
    }

    func get(url urlString: String, completion: @escaping (([String:Any])->())) {
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        let session = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (data != nil) {
                do {
                    if let result = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                        completion(result)
                    }
                } catch {
                    DispatchQueue.main.async { print("JSON Parsing error") }
                }
            }
        }
        
        DispatchQueue.global().sync {
            session.resume()
        }
    }
    
    func moedaInfoAlert(moeda: Moeda) {
        let alertMessage = "Nome: \(moeda.nome)" + "\n" + "Valor de compra: \(moeda.simbolo)\(moeda.valorDeCompra)"
        let alert = UIAlertController(title: "Informações da moeda", message: alertMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true)
    }
    
    @objc func consultarMoedaAction() {
        let indexMoedaSelecionada = self.moedasPickerView.selectedRow(inComponent: 0)
        
        get(url: "https://blockchain.info/ticker") { (json) in
            var moeda: Moeda?
            
            let key = self.pickerElements[indexMoedaSelecionada]
            let value = json[key] as! [String:Any]
            let buyValue = value["buy"] as! Double
            let simbol = value["symbol"] as! String
        
            DispatchQueue.main.sync {
                let buyValueStr = String(format: "%.2f", buyValue)
                moeda = Moeda(nome: key, valorDeCompra: buyValueStr, simbolo: simbol)
                self.moedaInfoAlert(moeda: Moeda(nome: moeda!.nome, valorDeCompra: moeda!.valorDeCompra, simbolo: moeda!.simbolo))
            }
            
        }
    }

}

extension ViewController : UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerElements.count == 0 {
            return 1
        }
        
        return pickerElements.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerElements.count == 0 {
            return "Loading info..."
        } else {
            return pickerElements[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "American Typewriter", size: 20)
            pickerLabel?.textAlignment = .center
        }
        if pickerElements.isEmpty {
            pickerLabel?.text = "Loading..."
        } else {
            pickerLabel?.text = pickerElements[row]
        }
        pickerLabel?.textColor = UIColor.yellow
        
        return pickerLabel!
    }
    
}

struct Moeda {
    let nome: String
    let valorDeCompra: String
    let simbolo: String
}

